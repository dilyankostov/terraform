resource "aws_instance" "haproxyInstance" {
  ami = "${var.image_id}"
  key_name = "${var.key}"
  instance_type = "t2.micro"
  tags {
    Name = "DMinstanceHaproxy"
    }
  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"
  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]
  connection {
          user = "ec2-user"
          type = "ssh"
          private_key = "${file(var.PATH_TO_PRIVATE_KEY)}"
          timeout = "2m"
      }
      provisioner "remote-exec" {
          inline = [
            "sleep 25",
            "sudo yum -y install haproxy",
            "sudo systemctl start haproxy",
            "sudo systemctl enable haproxy"
          ]
      }
      provisioner "file" {
        content     = "${data.template_file.haproxyconf.rendered}"
        destination = "/home/ec2-user/haproxy.cfg"
      }
      provisioner "remote-exec" {
          inline = [
            "sudo mv ~/haproxy.cfg /etc/haproxy/haproxy.cfg",
            "sudo systemctl restart haproxy"
          ]
      }
}
