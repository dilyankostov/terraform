data "aws_route53_zone" "selected" {
 name         = "grads.al-labs.co.uk."
 private_zone = false
}
resource "aws_route53_record" "www" {
 zone_id = "${data.aws_route53_zone.selected.zone_id}"
 name    = "www.dm.grads.al-labs.co.uk"
 type    = "A"
 ttl     = "300"
 records = ["${aws_instance.haproxyInstance.public_ip}"]
 depends_on = ["data.aws_route53_zone.selected"]
}
