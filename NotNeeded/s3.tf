terraform {
  backend "s3" {
    bucket = "${lower(var.my_tag)}-tfstore"
    key    = "terraform.tfstate"
    region = "var.region"
  }
}

data "terraform_remote_state" "simpleVM" {
  backend = "s3"
  config {
    bucket = "${lower(var.my_tag)}-tfstore"
    key    = "terraform.tfstate"
    region = "${var.region}"
  }
}
