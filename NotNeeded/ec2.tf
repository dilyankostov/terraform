resource "aws_instance" "example" {
  ami = "${var.image_id}"
  instance_type = "t2.micro"
  tags {
    Name = "DMinstance"
    }
  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"
  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]
  # the public SSH key
  key_name = "${var.key}"

  connection {
  user="${var.INSTANCE_USERNAME}"
  private_key="${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}
