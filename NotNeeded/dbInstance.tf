resource "aws_db_instance" "default" {
tags {
  Name = "DMdbInstance"
  }
  allocated_storage    = 5
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "dm_db_instance"
  username             = "DM"
  password             = "dmdmdmdmdm"
  parameter_group_name = "default.mysql5.7"
  depends_on = ["aws_db_subnet_group.default", "aws_db_security_group.default"]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  #security_group_names = ["${aws_db_security_group.default.name}"]
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]
}
