variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}
variable "image_id" {
  default = "ami-009d6802948d06e52"
}
variable "my_tag" {
  default = "dm"
  }
variable "PATH_TO_PRIVATE_KEY" {
default = "~/.aws/dm.pem"
}
variable "key" { default = "dm" }
variable "INSTANCE_USERNAME" { default = "ec2-user" }
