resource "aws_db_security_group" "default" {
  name = "dm_security_group_rds"
  ingress {
    cidr = "10.0.0.0/16"
  }
}
