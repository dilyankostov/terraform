#apacheInstance
resource "aws_instance" "apacheInstance" {
  ami = "${var.image_id}"
  key_name = "${var.key}"
  instance_type = "t2.micro"
  tags {
    Name = "DMinstanceApache"
    }
  # the VPC subnet
  subnet_id = "${aws_subnet.main-private-1.id}"
  # the security group
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]
  user_data = "${file("./shellscripts/apacheScript.sh")}"
}
