#Creating Virtual Private Cloud (subnets,internet gateway and route table)
# Internet VPC
resource "aws_vpc" "main" {
    cidr_block = "11.0.0.0/26"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    tags {
        Name = "DMvpc"
    }
}

# Subnets
resource "aws_subnet" "main-public-1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "11.0.0.0/28"
    map_public_ip_on_launch = "true"
    availability_zone = "${var.region}a"

    tags {
        Name = "DM_PublicSubnet"
    }
}

resource "aws_subnet" "main-private-1" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "11.0.0.16/28"
    map_public_ip_on_launch = "false"
    availability_zone = "${var.region}a"

    tags {
        Name = "DM_PrivateSubnet1"
    }
}

resource "aws_subnet" "main-private-2" {
    vpc_id = "${aws_vpc.main.id}"
    cidr_block = "11.0.0.32/28"
    map_public_ip_on_launch = "false"
    availability_zone = "${var.region}b"

    tags {
        Name = "DM_PrivateSubnet2"
    }
}


# Internet GW
resource "aws_internet_gateway" "main-gw" {
    vpc_id = "${aws_vpc.main.id}"

    tags {
        Name = "DMgateway"
    }
}

# route tables
resource "aws_route_table" "main-public" {
    vpc_id = "${aws_vpc.main.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.main-gw.id}"
    }

    tags {
        Name = "DMrouteTable"
    }
}

# route associations public
resource "aws_route_table_association" "main-public-1-a" {
    subnet_id = "${aws_subnet.main-public-1.id}"
    route_table_id = "${aws_route_table.main-public.id}"
}
