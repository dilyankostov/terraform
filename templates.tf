data "template_file" "haproxyconf" {
  template = "${file("${path.module}/haproxy.cfg.tpl")}"

  vars {
    nginx_priv_ip = "${aws_instance.nginxInstance.private_ip}"
    apache_priv_ip = "${aws_instance.apacheInstance.private_ip}"
  }
}
