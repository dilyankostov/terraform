## Creating Launch Configuration
resource "aws_launch_configuration" "example" {
  image_id               = "${var.image_id}"
  instance_type          = "t2.micro"
  security_groups        = ["${aws_security_group.allow-ssh.id}"]
  key_name               = "${var.key}"
  user_data = <<-EOF
              #!/bin/bash
              sudo yum install -y httpd
              sudo systemctl start httpd
              echo "Hello, World" > index.html
              sudo cp -f index.html /var/www/html/index.html
              EOF
  lifecycle {
    create_before_destroy = true
  }
}
## Creating AutoScaling Group
resource "aws_autoscaling_group" "example" {
  launch_configuration = "${aws_launch_configuration.example.id}"
  vpc_zone_identifier = ["${aws_subnet.main-public-1.id}"]
  min_size = 1
  max_size = 5
  load_balancers = ["${aws_elb.example.name}"]
  health_check_type = "ELB"
  tag {
    key = "Name"
    value = "dm_asg"
    propagate_at_launch = true
  }
}
