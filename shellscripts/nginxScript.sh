#!/bin/bash
set -xv
# sleep until instance is ready
# install nginx
# make sure nginx is started
sudo yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y install nginx
sudo systemctl start nginx

sudo systemctl enable nginx
