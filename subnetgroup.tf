resource "aws_db_subnet_group" "default" {
  name       = "dm_aws_db_subnet_group"
  subnet_ids = ["${aws_subnet.main-private-1.id}", "${aws_subnet.main-private-2.id}"]
  tags {
    Name = "DM_DB_PrivsubnetGroup"
  }
}
