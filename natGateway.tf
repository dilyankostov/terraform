#elasticIP
resource "aws_eip" "tuto_eip" {
 vpc = true
 depends_on = ["aws_internet_gateway.main-gw"]
}

#natGateway
resource "aws_nat_gateway" "nat" {
   allocation_id = "${aws_eip.tuto_eip.id}"
   subnet_id = "${aws_subnet.main-public-1.id}"
   depends_on = ["aws_internet_gateway.main-gw"]
   tags {
       Name = "DMnatGateway"
   }
}

#routeTable
resource "aws_route_table" "private_route_table" {
    vpc_id = "${aws_vpc.main.id}"

    tags {
        Name = "DMprivate_route_table"
    }
}
#route inside route table for private subnet
resource "aws_route" "private_route" {
	route_table_id  = "${aws_route_table.private_route_table.id}"
	destination_cidr_block = "0.0.0.0/0"
	nat_gateway_id = "${aws_nat_gateway.nat.id}"
}

# Associate subnet private_1_subnet_eu_west_2a to private route table
resource "aws_route_table_association" "pr_1_subnet_association" {
    subnet_id = "${aws_subnet.main-private-1.id}"
    route_table_id = "${aws_route_table.private_route_table.id}"
}

# Associate subnet private_2_subnet_eu_west_2b to private route table
resource "aws_route_table_association" "pr_2_subnet_association" {
    subnet_id = "${aws_subnet.main-private-2.id}"
    route_table_id = "${aws_route_table.private_route_table.id}"
}
