### Creating ELB
resource "aws_elb" "example" {
  name = "dmELB"
  security_groups = ["${aws_security_group.allow-ssh.id}"]
  subnets = ["${aws_subnet.main-public-1.id}"]
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}
